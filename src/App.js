// import $ from 'jquery';
import logo from './logo.svg';
import './App.css';
import React, { useState, useEffect } from 'react';

function App() {
  const [currentTime, setCurrentTime] = useState(0);

  useEffect(() => {
    fetch('/apis/time').then(res => res.json()).then(data => {
      setCurrentTime(data.time);
    });
  }, []);

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
         Clock is ticking...
        </p>
        <p>
          {currentTime}
        </p>
        {/* <button id="checkout-button" onClick={handleClick}>Checkout</button> */}

      </header>
    </div>
  );
}

export default App;
